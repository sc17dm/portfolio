---
layout: blocks
title: Homepage
date: 2017-11-22T23:00:00.000+00:00
page_sections:
- template: simple-header
  block: header-3
  logo: https://gitlab.com/sc17dm/portfolio/-/raw/master/_assets/2020/11/21/bluespidey.jpg
- template: 1-column-text
  block: one-column-1
  headline: Portfolio
  content: Hello! I am Diana Marin, a fourth year student in Computer Science with
    High <br>Performance Graphics and Games Engineering at the University of Leeds.<br><br>This
    is a portofolio of current and previous projects  created as an assignment<br>or
    during free time. Projects developed under MD Lab Games can be found at<br><a
    href="https://mdlabgames.com/" title="mdlabgames.com">mdlabgames.com</a>.
  slug: ''
- template: content-feature
  block: feature-1
  media_alignment: Left
  slug: swap
  headline: "<strong>Loop Subdivision Surfaces</strong>"
  content: An application that can subdivide a given surface that is assumed to be
    manifold, using Loop subdivision, to an arbitrary level, using C++. The program
    uses the directed-edge data structure and recomputes in linear time each level
    of the subdivided mesh, without the need of a polynomial time reconstruction of
    the directed-edge data structure.<br>The illustration consists of (from left to
    right, then top to bottom) original object, 1 level of subdivision, 2 levels,
    3 levels, 4 levels, all with flat normals and 4 levels of subdivision with smooth
    normals.
  media:
    image: https://gitlab.com/sc17dm/portfolio/-/raw/master/_assets/2020/11/30/5bout.png
    alt_text: uBuild Blocks Mock-Up
- template: content-feature
  block: feature-1
  media_alignment: Right
  slug: ''
  headline: "<strong>FakeGL</strong>"
  content: This project was aimed at replicating fixed function OpenGL basic functionality.
    I have implemented matrix stacks, vertex transformations, points, line and triangles
    and primitives, textures, rasterization and fragment processing. The application
    includes depth testing and lighting, with both Gouraud and Phong shading.<br>The
    illustration presents a basic sphere, with Gouraud shading and depth testing.
    There is a difference between the light intensity that is caused by gamma correction.
  media:
    image: https://gitlab.com/sc17dm/portfolio/-/raw/master/_assets/2020/11/30/fakegl_0.png
    alt_text: Customize Blocks
- template: content-feature
  block: feature-1
  media_alignment: Left
  headline: "<strong>Inverse Kinematics Solver</strong>"
  content: This application implements basic animation using BVH files. It can apply
    forward kinematics, play an animation at a given rate and modify drawing parameters,
    like joint and bone radius. It also implements Inverse Kinematics, by using three
    methods for inverting the Jacobian, pseudo-inverse, damped IK and control joints.<br><br>The
    illustration presents first the original mesh, at one frame during the walking
    motion. Secondly, the left hand is dragged upwards and pseudo-inverse IK is applied,
    resulting in the second image. On the second row, the first image is the result
    of damped IK applied to move the left leg outwards. The final picture represents
    IK using control joints, namely, the blue joint is set as control an it should
    not chane its rotation too much, while the left hand is dragged downwards. This
    is applied in place because the movement of the root, since it affects the final
    movement the most, tends to be preferred and the other joints do not move as much.
  slug: ''
  media:
    image: https://gitlab.com/sc17dm/portfolio/-/raw/master/_assets/2020/11/30/ik.png
    alt_text: ''
- template: content-feature
  block: feature-1
  media_alignment: Right
  headline: "<strong>Free Form Deformation</strong>"
  content: I have implemented a cage-based free form deformation that can use rectangular
    and triangular grids, as well as attenuation based deformation based on distance.
    The deformation is done by picking and pulling the vertices .<br>The illustration
    contains the original 2D mesh, enclosed with a rectangular mesh, then a deformation
    applied to the case and how it affects the original mesh, followed by an attenuated
    deformation and its effect on the mesh. The second row consists of a mesh enclosed
    with a triangular cage, with randomised control points and Dealunay triangulated,
    then a deformation applied to the cage, followed by a rectangular grid with deformation
    applied in 3D.
  slug: ''
  media:
    image: https://gitlab.com/sc17dm/portfolio/-/raw/master/_assets/2020/11/30/ffd.png
    alt_text: ''
- template: content-feature
  block: feature-1
  media_alignment: Left
  headline: "<strong>Point Set Rendering</strong>"
  content: This was my third year project and it implemented the Computing and Rendering
    Point Set Surfaces by Marc Alexa et al. The final results were close to the desired
    surface, exhibiting smoother surfaces than triangulated meshes. However, manual
    implmenetation of the numerical algorithms led to numerical errors and holes in
    the surface.<br><br>Some results are presented in the picture. From left to right,
    the first row presents the grid of octrees necessary for point clustering (optimisation
    of spacial neighbourhood), the computed normals for the Stanford Bunny and the
    final surface. On the second row, the Buddha statue has been used and the normals
    are presented, followed by the surface and a close-up with a small patch size
    to illustrate the procedure.
  slug: ''
  media:
    image: https://gitlab.com/sc17dm/portfolio/-/raw/master/_assets/2020/11/30/psr-1.png
    alt_text: ''
- template: content-feature
  block: feature-1
  media_alignment: Right
  headline: "<strong>Manifold Checker</strong>"
  content: This project implements the half-edge data structure and uses it to check
    if 3D meshes, given as triangle soup, are manifold or not. However, self-intersection
    is not checked in this application, but the object is broken into connected components
    for determining manifold-ness and also, computing the genus if possble.<br><br>Some
    of the test files have been provided as a rendering, along with some of the output
    from the manifold checker.
  slug: ''
  media:
    image: https://gitlab.com/sc17dm/portfolio/-/raw/master/_assets/2020/11/30/manifold-1.png
    alt_text: ''
- template: content-feature
  block: feature-1
  media_alignment: Left
  headline: "<strong>Cloth Simulation</strong>"
  content: 'Work in progress. <br><br>This application should be able to simulate
    a cloth in different scenarios: falling on ground, falling with fixed corners,
    falling on a rotating sphere with friction and falling with fixed corners and
    applied wind.'
  slug: ''
  media:
    image: ''
    alt_text: ''
- template: simple-footer
  block: footer-1
  content: "© Diana Marin &amp; Forestry (for the Jekyll template)"

---
